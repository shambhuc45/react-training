import React, { useState } from "react";
import ChildOpt1 from "./ChildOpt1.jsx";

const ParentOpt1 = () => {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("Shambhu");
  console.log("I am Parent");
  return (
    <div>
      <ChildOpt1 name={name}></ChildOpt1>
      <br />
      count is :{count}
      <br />
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment Count
      </button>
      <br />
      Name is :{name}
      <br />
      <button
        onClick={() => {
          setName("Ram");
        }}
      >
        Change Name
      </button>
    </div>
  );
};

export default ParentOpt1;
