import React, { useMemo, useState } from "react";

const ParentMemo = () => {
  let [count, setCount] = useState(0);
  let [name, setName] = useState("Shambhu");
  let [surName, setSurName] = useState("Chaudhary");

  //   let fullName = name + surName;

  let fullName = useMemo(() => {
    return name + surName;
  }, [name, surName]);
  return (
    <div>
      count is {count}
      <br />
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
      <br />
      FullName is : {fullName}
      <br />
      <button
        onClick={() => {
          setName("Ram");
        }}
      >
        Change Name
      </button>
      <button
        onClick={() => {
          setSurName("KC");
        }}
      >
        Change Surname
      </button>
    </div>
  );
};

export default ParentMemo;
