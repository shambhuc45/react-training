import React, { useCallback, useState } from "react";
import CallbackChild from "./CallbackChild.jsx";

const CallbackParent = () => {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("Shambhu");
  console.log("I am Parent");

  // const function1 = () => {
  //   console.log("I am function");
  // };
  // Don't define functions like this instead use useCallback

  const function1 = useCallback(() => {
    console.log("I am function");
  }, [count]);

  // Parent
  // 1st render new function1 is created ( thus Child component will render)
  // from 2nd render
  // new function1 is created only if count is change (thus Child component )
  // if count doesn't change it will take pre  function (child will not render)

  return (
    <div>
      <CallbackChild function1={function1}></CallbackChild>
      <br />
      count is :{count}
      <br />
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment Count
      </button>
      <br />
      Name is :{name}
      <br />
      <button
        onClick={() => {
          setName("Ram");
        }}
      >
        Change Name
      </button>
    </div>
  );
};

export default CallbackParent;
