import React, { memo } from "react";

const CallbackChild = ({ name }) => {
  console.log("Child ho hai ma");
  return <div>{name}</div>;
};

export default memo(CallbackChild);
