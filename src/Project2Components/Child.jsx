import React from "react";

const Child = ({ setData }) => {
  let name = "Shambhu";

  return (
    <div>
      Child
      <button
        onClick={() => {
          setData(name);
        }}
      >
        Send
      </button>
    </div>
  );
};

export default Child;
