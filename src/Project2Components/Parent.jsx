import React, { useState } from "react";
import Child from "./Child.jsx";

const Parent = () => {
  let [data, setData] = useState("");
  return (
    <div>
      <Child data={data} setData={setData}></Child>
      <p>{data}</p>
    </div>
  );
};

export default Parent;
