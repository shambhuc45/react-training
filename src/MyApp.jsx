import React, { createContext, useState } from "react";
import MyRoutes from "./myComponents/Router/MyRoutes";
// import Router from "./myProject/Router/Router.js";

export let GlobalVariableContext = createContext();
const MyApp = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(localStorage.getItem("token"));
  return (
    <>
      <GlobalVariableContext.Provider
        value={{ isLoggedIn: isLoggedIn, setIsLoggedIn: setIsLoggedIn }}
      >
        {/* <Router></Router> */}
        <MyRoutes></MyRoutes>
      </GlobalVariableContext.Provider>
    </>
  );
};

export default MyApp;
