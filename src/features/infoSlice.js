import { createSlice } from "@reduxjs/toolkit";

const initialStateValue = {
  name: "Shambhu Chaudhary",
  gender: "male",
};

export const infoSlice = createSlice({
  name: "infoSlice",
  initialState: initialStateValue,
  reducers: {
    changeName: (state, action) => {
      state.name = action.payload;
    },
    changeGender: (state, action) => {
      state.gender = action.payload;
    },
  },
});
export const { changeName, changeGender } = infoSlice.actions;
export default infoSlice.reducer;
