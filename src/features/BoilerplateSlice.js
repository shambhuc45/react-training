import { createSlice } from "@reduxjs/toolkit";

const initialStateValue = {
  count: 0,
};

export const exampleSlice = createSlice({
  name: "exampleSlice",
  initialState: initialStateValue,
  reducers: {},
});
export const {} = exampleSlice.actions;
export default exampleSlice.reducer;
