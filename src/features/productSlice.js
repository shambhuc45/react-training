import { createSlice } from "@reduxjs/toolkit";

const initialStateValue = {
  productName: "demo product",
  price: 200,
};

export const productSlice = createSlice({
  name: "productSlice",
  initialState: initialStateValue,
  reducers: {
    changeProductName: (state, action) => {
      state.productName = action.payload;
    },
    changePrice: (state, action) => {
      state.price = action.payload;
    },
  },
});
export const { changeProductName, changePrice } = productSlice.actions;
export default productSlice.reducer;
