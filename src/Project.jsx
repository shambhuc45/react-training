import React from "react";
import Router from "./myProject/Router/Router.js";
import { useDispatch, useSelector } from "react-redux";
import { changeGender, changeName } from "./features/infoSlice.js";
import { changeCity, changeProvince } from "./features/addressSlice.js";
import { changePrice, changeProductName } from "./features/productSlice.js";

const Project = () => {
  //useSelector to retrieve data from the store.
  let infoData = useSelector((store) => {
    return store.info;
  });
  let addressData = useSelector((store) => {
    return store.address;
  });
  let productData = useSelector((store) => {
    return store.product;
  });
  // console.log(productData);
  let dispatch = useDispatch();
  return (
    <>
      <Router></Router>

      <p>Name is : {infoData.name}</p>
      <p>Gender is : {infoData.gender}</p>

      <p>City is : {addressData.city}</p>
      <p>Province is : {addressData.province}</p>
      <p>Product is : {productData.productName}</p>
      <p>Price is : ${productData.price}</p>
      <button
        onClick={() => {
          dispatch(changeName("Chaudhary"));
        }}
      >
        Change Name
      </button>
      <button
        onClick={() => {
          dispatch(changeGender("female"));
        }}
      >
        Change Gender
      </button>
      <br />
      <button
        onClick={() => {
          dispatch(changeCity("Kathmandu"));
        }}
      >
        Change City
      </button>
      <button
        onClick={() => {
          dispatch(changeProvince("Bagmati"));
        }}
      >
        Change Province
      </button>
      <br />
      <button
        onClick={() => {
          dispatch(changeProductName("Product1"));
        }}
      >
        Change Product Name
      </button>
      <button
        onClick={() => {
          dispatch(changePrice(400));
        }}
      >
        Change Price
      </button>
    </>
  );
};

export default Project;

/* 

localhost:3000/products  //read all  
localhost:3000/products/create   //form to add product

buttons in read all products(
    -localhost:3000/products/:id  //read specific
    -localhost:3000/products/update/:id   //form to update product
    -delete
)
*/
