import React from "react";
// import ParentOpt1 from "./Project2Components/optimization/ParentOpt1.jsx";
// import LearnUseReducer from "./components/learnUseReducer/learnUseReducer.jsx";
// import LearnUseReducer1 from "./components/learnUseReducer/LearnUseReducer1.jsx";
// import CallbackParent from "./Project2Components/optimization/learnUseCallback/CallbackParent.jsx";
// import ParentMemo from "./Project2Components/optimization/learnUseMemo/ParentMemo.jsx";
// import BasicTable from "./components/learnReactTable/BasicTable.jsx";
// import SortingTable from "./components/learnReactTable/SortingTable.jsx";
// import FilteringTable from "./components/learnReactTable/FilteringTable.jsx";
// import PaginationTable from "./components/learnReactTable/PaginationTable.jsx";
// import RowSelection from "./components/learnReactTable/RowSelection.jsx";
import ColumnOrder from "./components/learnReactTable/ColumnOrder.jsx";

const Project2 = () => {
  return (
    <div>
      {/* <Parent></Parent> */}
      {/* <LearnUseReducer></LearnUseReducer> */}
      {/* <LearnUseReducer1></LearnUseReducer1> */}
      {/* <ParentOpt1></ParentOpt1> */}
      {/* <CallbackParent></CallbackParent> */}
      {/* <ParentMemo></ParentMemo> */}
      {/* <BasicTable /> */}
      {/* <SortingTable></SortingTable> */}
      {/* <FilteringTable></FilteringTable> */}
      {/* <PaginationTable></PaginationTable> */}
      {/* <RowSelection></RowSelection> */}
      <ColumnOrder></ColumnOrder>
    </div>
  );
};

export default Project2;
