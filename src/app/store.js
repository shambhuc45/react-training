import { configureStore } from "@reduxjs/toolkit";
import counterSlice from "../features/infoSlice.js";
import addressSlice from "../features/addressSlice.js";
import productSlice from "../features/productSlice.js";
import { productApi } from "../myProject/Services/api/productService.js";
import { reviewApi } from "../myProject/Services/api/reviewService.js";

export const store = configureStore({
  reducer: {
    info: counterSlice,
    address: addressSlice,
    product: productSlice,

    [productApi.reducerPath]: productApi.reducer,
    [reviewApi.reducerPath]: reviewApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      [productApi.middleware],
      [reviewApi.middleware]
    ),
});
