import React from "react";

const EffectOfData = () => {
  let name = "Shambhu";
  let age = 100;
  let isMarried = false; //Boolean are not displayed in browser
  let tags = [<div>Hello1</div>, <div>Hello2</div>, <div>Hello3</div>];
  let info = { name: "shambhu", age: 100 };
  let a = null;
  let b = undefined;
  return (
    <div style={{ backgroundColor: "red" }}>
      <p>{name}</p>
      <p>{age}</p>
      <p>{isMarried}</p>
      <p>{tags}</p>
      {/* <p>{info}</p> */}
      <p>{info.name}</p>
      <p>{info.age}</p>
      <p>{a}</p>
      <p>{b}</p>
    </div>
  );
};

export default EffectOfData;
/* 
Boolean are not displayed in browser.
When array is placed in the browser => it place element one by (note [] and, are not written in browser)
We cannot place object in the browser (Objedct are not valid as a react child)
*/
