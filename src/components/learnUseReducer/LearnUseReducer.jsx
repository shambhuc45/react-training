import React, { useReducer } from "react";

// Use useReducer for non primitive type variable declaration

//rendering on useReducer
//useState = component will render when state variable changes
//useReducer = component will render if dispatch is called

const LearnUseReducer = () => {
  let reducer = (state, action) => {
    return action;
  };
  let [name, dispatch] = useReducer(reducer, "Shambhu");
  return (
    <div>
      {name}
      <button
        onClick={() => {
          dispatch("Chaudhary");
        }}
      >
        Change Name
      </button>
    </div>
  );
};

export default LearnUseReducer;
