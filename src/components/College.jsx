import React from "react";

const College = (props) => {
  return (
    <div>
      <p>College name is {props.name}</p>
      <p>cost was {props.cost}</p>
      <p>Address: {props.address}</p>
    </div>
  );
};

export default College;
