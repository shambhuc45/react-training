import React from "react";

const ButtonClick = () => {
  return (
    <div>
      <button
        onClick={() => {
          console.log("Clicked");
        }}
      >
        Click Me
      </button>
    </div>
  );
};

export default ButtonClick;
