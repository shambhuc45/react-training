import React, { useState } from "react";

const LearnUseState = () => {
  let [age, setAge] = useState(12);
  let [name, setName] = useState("Aalu");
  return (
    <>
      {age}
      <br />
      {name}
      <br />
      <button
        onClick={() => {
          setAge(age + 1);
        }}
      >
        Click Me
      </button>
      <button
        onClick={() => {
          setName("Shambhu");
        }}
      >
         Change Name
      </button>
    </>
  );
};

export default LearnUseState;
