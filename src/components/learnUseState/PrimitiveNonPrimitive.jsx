import React, { useState } from "react";

const PrimitiveNonPrimitive = () => {
  let [count, setCount] = useState(1);
  let [list, setList] = useState([1, 2, 3]);
  console.log("hello");
  return (
    <>
      <button
        onClick={() => {
          setCount(1);
          // the state variable must change to re render the page.
        }}
      >
        Change count
      </button>
      <button
        onClick={() => {
          setList([1, 2, 3, 4]);
        }}
      >
        Change List
      </button>
    </>
  );
};

export default PrimitiveNonPrimitive;
