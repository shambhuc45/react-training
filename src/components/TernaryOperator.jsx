import React from "react";

const TernaryOperator = () => {
  let age = 10;
  return <>{age >= 18 ? <div>Adult</div> : <div>Underage</div>}</>;
};

export default TernaryOperator;
