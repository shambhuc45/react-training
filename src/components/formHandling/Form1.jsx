import React from "react";

const Form1 = () => {
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Submitted");
  };
  return (
    <>
      <p>Form Handling Tutorials</p>
      <form onSubmit={handleSubmit}>
        {/* Inputs  */}
        <input type="text" placeholder="FullName" />
        <br />
        <input type="email" placeholder="Email" />
        <br />
        <input type="number" placeholder="Age" />
        <br />
        <input type="radio"></input>
        <br />
        <input type="radio"></input>
        <br />
        <label htmlFor="hobbies"></label>
        <input type="checkbox" id="hobbies"></input>
        <br />
        <input type="checkbox" id="hobbies"></input>
        <br />
        <input type="tel" placeholder="Phone" />
        <br />
        <input type="date" />
        <br />
        <input type="file" /> <br />
        <input type="color" name="" id="" />
        <br />
        {/* textareas  */}
        <textarea name="" id="" rows="5"></textarea>
        <br />
        {/* Select  */}
        <select name="country" id="">
          <option value="nepal">Nepal</option>
          <option value="china">China</option>
          <option value="pakistan">Pakistan</option>
        </select>
        <br />
        <button type="submit">Submit</button>
      </form>
    </>
  );
};

export default Form1;
