import React, { useState } from "react";

const Form2 = () => {
  const [fullname, setFullname] = useState("");
  const [address, setAddress] = useState("");
  const [email, setEmail] = useState("");
  const [description, setDescription] = useState("");
  const [isMarried, setIsMarried] = useState(false);
  const [country, setCountry] = useState("1");
  const [gender, setGender] = useState("male");

  let countries = [
    { name: "Select Country", _id: "1", disabled: true },
    { name: "China", _id: "2" },
    { name: "India", _id: "3" },
    { name: "Bhutan", _id: "4" },
    { name: "Nepal", _id: "5" },
  ];
  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Others", value: "others" },
  ];

  const handleSubmit = (e) => {
    e.preventDefault();

    let data = {
      fullname: fullname,
      address: address,
      email: email,
      description: description,
      isMarried: isMarried,
      country: country,
      gender: gender,
    };
    console.log("Submitted", data);
  };
  return (
    <>
      <form action="" onSubmit={handleSubmit}>
        <div style={{ marginTop: "5px" }}>
          <label htmlFor="fullname">Full Name:</label>
          <input
            type="text"
            id="fullname"
            value={fullname}
            onChange={(e) => {
              setFullname(e.target.value);
            }}
          />
        </div>
        <div style={{ marginTop: "5px" }}>
          <label htmlFor="address">Address:</label>
          <input
            type="text"
            id="address"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          />
        </div>
        <div style={{ marginTop: "5px" }}>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
        </div>
        <div style={{ marginTop: "5px" }}>
          <label htmlFor="description">Description:</label>
          <textarea
            id="description"
            rows="4"
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
          ></textarea>
        </div>
        <div style={{ marginTop: "5px" }}>
          <input
            type="checkbox"
            id="isMarried"
            checked={isMarried}
            onChange={(e) => {
              setIsMarried(e.target.checked);
            }}
          />
          <label htmlFor="isMarried">Is Married ?</label>
        </div>
        <div>
          <label htmlFor="country">Select Your country</label>
          <select
            name="country"
            id="country"
            value={country}
            onChange={(e) => {
              setCountry(e.target.value);
            }}
          >
            {/* <option value="nepal">Nepal</option>
            <option value="china">China</option>
            <option value="pakistan">Pakistan</option>
            <option value="bangladesh">Bangladesh</option>
            <option value="australia">Australia</option> */}
            {countries.map((country, i) => {
              return (
                <option
                  key={country._id}
                  value={country._id}
                  disabled={country.disabled}
                >
                  {country.name}
                </option>
              );
            })}
          </select>
        </div>
        <div style={{ marginTop: "5px" }}>
          <label htmlFor="male">Gender :</label>
          <br />

          {/* <input
            type="radio"
            id="male"
            value="male"
            checked={gender === "male"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          />
          <label htmlFor="male">Male</label>
          <input
            type="radio"
            id="female"
            value="female"
            checked={gender === "female"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          />
          <label htmlFor="female">Female</label>
          <input
            type="radio"
            id="other"
            value="other"
            checked={gender === "other"}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          />
          <label htmlFor="other">Other</label> */}
          {genders.map((item, i) => {
            return (
              <span key={i}>
                <input
                  type="radio"
                  id={item.value}
                  value={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                />
                <label htmlFor={item.value}>{item.label}</label>
              </span>
            );
          })}
        </div>
        <button type="submit">Submit</button>
      </form>
    </>
  );
};

export default Form2;
