import ColumnFilter from "./ColumFilter.jsx";

export const COLUMNS = [
  {
    Header: "Id",
    accessor: "id",
    Filter: ColumnFilter,
    disableFilters: true,
  },
  {
    Header: "Full Name",
    accessor: "fullName",
    Filter: ColumnFilter,
  },
  {
    Header: "Email",
    accessor: "email",
    Filter: ColumnFilter,
  },
  {
    Header: "Gender",
    accessor: "gender",
    Filter: ColumnFilter,
  },
  {
    Header: "Address",
    accessor: "address",
    Filter: ColumnFilter,
  },
  {
    Header: "Phone Number",
    accessor: "phoneNumber",
    Filter: ColumnFilter,
  },
];

export const Grouped_Column = [
  {
    Header: "Id",
    accessor: "id",
  },
  {
    Header: "FullName and Email",
    columns: [
      {
        Header: "Full Name",
        accessor: "fullName",
      },
      {
        Header: "Email",
        accessor: "email",
      },
    ],
  },
  {
    Header: "Info",
    columns: [
      {
        Header: "Gender",
        accessor: "gender",
      },
      {
        Header: "Address",
        accessor: "address",
      },
      {
        Header: "Phone Number",
        accessor: "phoneNumber",
      },
    ],
  },
];
