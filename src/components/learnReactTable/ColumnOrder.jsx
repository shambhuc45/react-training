import React, { useMemo } from "react";
import { useColumnOrder, useTable } from "react-table";
import MOCK_DATA from "./data/MOCK_DATA.json";
import { COLUMNS } from "./data/columns.js";
import "./table.css";

const ColumnOrder = () => {
  const columns = useMemo(() => {
    return COLUMNS;
  }, []);
  const data = useMemo(() => {
    return MOCK_DATA;
  }, []);
  const tableInstance = useTable(
    {
      columns: columns,
      data: data,
    },
    useColumnOrder
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    setColumnOrder,
  } = tableInstance;

  const changeOrder = () => {
    setColumnOrder([
      "id",
      "fullName",
      "email",
      "phoneNumber",
      "address",
      "gender",
    ]);
  };
  return (
    <>
      <button
        onClick={() => {
          changeOrder();
        }}
      >
        Change Column Order
      </button>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => {
            return (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => {
                  return (
                    <th {...column.getHeaderProps()}>
                      {column.render("Header")}
                    </th>
                  );
                })}
              </tr>
            );
          })}
        </thead>
        <tbody {...getTableBodyProps}>
          {rows.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
};

export default ColumnOrder;
