import React from "react";

const RemoveLocalStorageData = () => {
  return (
    <div>
      <button
        onClick={() => {
          localStorage.removeItem("token");
        }}
      >
        Remove token
      </button>
    </div>
  );
};

export default RemoveLocalStorageData;
