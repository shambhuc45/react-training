import React, { useContext } from "react";
import TestContext from "./context/TestContext.js";

const GrandChild = () => {
  const { name, setName } = useContext(TestContext);
  console.log(name);
  return (
    <div>
      {name}
      <button
        onClick={() => {
          setName("Shambhu Chaudhary");
        }}
      >
        Change Name
      </button>
    </div>
  );
};

export default GrandChild;
