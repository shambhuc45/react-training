import React, { useState } from "react";
import TestContext from "./TestContext.js";
import GrandChild from "../GrandChild.jsx";

const TestContextProvider = () => {
  const [name, setName] = useState("Shambhu");
  const [email, setEmail] = useState("");
  return (
    <>
      <TestContext.Provider value={{ name, setName, email, setEmail }}>
        <GrandChild></GrandChild>
      </TestContext.Provider>
    </>
  );
};

export default TestContextProvider;
