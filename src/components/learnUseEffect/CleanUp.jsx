import React, { useEffect, useState } from "react";

const CleanUp = () => {
  let [count, setCount] = useState(0);

  // useEffect(() => {
  //   console.log(count);

  //   return () => {
  //     console.log("i am cleanUp function");
  //   };
  // }, [count]);

  useEffect(() => {
    let interval1 = setInterval(() => {
      console.log("Hello World");
    }, 2000);

    return () => {
      clearInterval(interval1);
    };
  }, []);
  return (
    <div style={{ border: "1px solid blue" }}>
      count is:{count}
      <br />
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default CleanUp;

/* cleanup function is nothing but a function returned by useEffect
cleanUp function doesn't run on first render
During execution of useEffect on second render, cleanup function gets executed first
Component Mount (first render)
    All code except cleanUp function

Component Update:
    when changing the state variable

Component Unmount:
    not execute but clean up function gets executed



Explain useEffect
Explain cleanUp function
lifecycle of Component


Component DidMount (first render)
    All code except cleanUp function

Component DidUpdate:(second render)
    when changing the state variable

Component DidUnmount:
    not execute but clean up function gets executed
 */
