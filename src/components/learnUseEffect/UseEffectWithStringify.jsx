import React, { useEffect, useState } from "react";

const UseEffectWithStringify = () => {
  let [name, setName] = useState("Shambhu");
  let [list, setList] = useState([2, 3]);

  useEffect(() => {
    console.log(name);
    console.log(list);
  }, [name, JSON.stringify(list)]);
  return <div>UseEffectWithStringify</div>;
};

export default UseEffectWithStringify;

//if dependency is non Primitive use JSON.stringify()   for better performance
