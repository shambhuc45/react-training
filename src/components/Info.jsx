import React from "react";

const Info = ({ name, age, address }) => {
  return (
    <>
      <p>Name is {name}</p>
      <p>Age is {age}</p> 
      <p>Address is {address}</p>
    </>
  );
};

export default Info;
