import React, { useRef } from "react";
// import PrimitiveNonPrimitive from "./components/learnUseState/PrimitiveNonPrimitive.jsx";
// import CleanUp from "./components/learnUseEffect/CleanUp.jsx";
// import AddDataToSessionStorage from "./components/LearnSessionStorage/AddDataToSessionStorage";
// import GetDataFromSessionStorage from "./components/LearnSessionStorage/GetDataFromSessionStorage";
// import LearnUseState from "./components/learnUseState/LearnUseState";
// import Form1 from "./components/formHandling/Form1";
// import Form2 from "./components/formHandling/Form2";
// import MyNavbar from "./myComponents/MyNavbar";
// import MyRoutes from "./myComponents/MyRoutes";
// import FormikForm from "./myComponents/formik/FormikForm";
// import TestContextProvider from "./components/learnContextApi/context/TestContextProvider.jsx";
// import GrandChild from "./components/learnContextApi/GrandChild.jsx";
// import LearnJSONStrParse from "./components/LearnJSONStrParse/LearnJSONStrParse.jsx";
// import FormikTutorial from "./myComponents/formik/FormikTutorial";
// import AddItemToLocalStorage from "./components/LearnLocalStorage/AddItemToLocalStorage";
// import GetLocalStorageData from "./components/LearnLocalStorage/GetLocalStorageData";
// import RemoveLocalStorageData from "./components/LearnLocalStorage/RemoveLocalStorageData";
// import Info from "./components/Info";
// import College from "./components/College";
// import Style from "./components/Style";
// import EffectOfData from "./components/EffectOfData";
// import Age from "./components/Age";
// import TernaryOperator from "./components/TernaryOperator";
// import ButtonClick from "./components/ButtonClick";

const App = () => {
  // const [show, setShow] = useState(true);
  let ref1 = useRef();
  let ref2 = useRef();
  let ref3 = useRef();
  const handleClick = () => {
    ref1.current.style.color = "red";
    ref2.current.style.color = "green";
  };
  const handleManipulate = () => {
    ref3.current.focus();
  };
  return (
    <div>
      {/* <Info name="Shambhu Chaudhary" age={100} address="Gwarko"></Info>
      <College name="OCEM" cost={595000} address="Gaidakot"></College>
      <Style></Style>
      <EffectOfData></EffectOfData>
      <Age></Age>
      <TernaryOperator></TernaryOperator>
      <ButtonClick></ButtonClick> */}
      {/* <LearnUseState></LearnUseState> */}
      {/* <Form1></Form1> */}
      {/* <Form2></Form2> */}
      {/* <MyNavbar></MyNavbar> */}
      {/* <MyRoutes></MyRoutes> */}

      {/* <FormikForm></FormikForm> */}

      {/* <FormikTutorial></FormikTutorial> */}
      {/* <AddItemToLocalStorage></AddItemToLocalStorage> */}
      {/* <GetLocalStorageData></GetLocalStorageData> */}
      {/* <RemoveLocalStorageData></RemoveLocalStorageData> */}
      {/* <AddDataToSessionStorage></AddDataToSessionStorage> */}
      {/* <GetDataFromSessionStorage></GetDataFromSessionStorage> */}
      {/* <TestContextProvider>
        <GrandChild></GrandChild>
      </TestContextProvider> */}
      {/* <LearnJSONStrParse></LearnJSONStrParse> */}
      {/* <PrimitiveNonPrimitive></PrimitiveNonPrimitive> */}
      {/* {show ? <CleanUp ></CleanUp> : <></>}
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <button
        onClick={() => {
          setShow(true);
        }}
      >
        Show Component
      </button>
      <button
        onClick={() => {
          setShow(false);
        }}
      >
        Hide Component
      </button> */}

      <p ref={ref1}> Paragraph 1</p>
      <p ref={ref2}> Paragraph 2</p>
      <button onClick={handleClick}>Change Color</button>

      <br />
      <br />
      <br />

      <div onClick={handleManipulate}>paragraph element to focus</div>
      <input type="text" ref={ref3} />
    </div>
  );
};

export default App;
