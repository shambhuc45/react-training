import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { hitApi } from "../Services/hitApi.js";
import { toast } from "react-toastify";

const MySwal = withReactContent(Swal);

let showAlert = async (get, url) => {
  try {
    let response = await MySwal.fire({
      title: "Confirmation",
      text: "Are you sure you want to delete?",
      icon: "question",
      showCancelButton: true,
      confirmButtonText: "OK",
      cancelButtonText: "Cancel",
    });
    if (response.isConfirmed) {
      try {
        let response = await hitApi({
          url: url,
          method: "DELETE",
        });
        toast.success(response.data.message);
        get();
      } catch (error) {
        console.log(error.message);
      }
    }
  } catch (error) {
    console.log(error.message);
  }
};

export default showAlert;
