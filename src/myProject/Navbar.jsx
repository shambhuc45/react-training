import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <div>
      <nav>
        <NavLink
          to="/products"
          style={{ marginRight: "15px", textDecoration: "none" }}
        >
          Products
        </NavLink>
        <NavLink
          to="/products/create"
          style={{ marginRight: "15px", textDecoration: "none" }}
        >
          Create Product
        </NavLink>
      </nav>
    </div>
  );
};

export default Navbar;
