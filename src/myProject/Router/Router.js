import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import Footer from "../Footer.jsx";
import Navbar from "../Navbar.jsx";
import AllProductsUsingRtk from "../Product/AllProductsUsingRtk.jsx";
import CreateProductUsingRtk from "../Product/CreateProductUsingRtk.jsx";
import ProductDetail from "../Product/ProductDetail.jsx";
import UpdateProductUsingRtk from "../Product/UpdateProductUsingRtk.jsx";

const Router = () => {
  return (
    <>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <Navbar></Navbar>
              <Outlet></Outlet>
              <Footer></Footer>
            </div>
          }
        >
          <Route index element={<div>This is Landing Page</div>}></Route>
          <Route
            path="products"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            {/* <Route index element={<AllProducts></AllProducts>}></Route> */}
            <Route
              index
              element={<AllProductsUsingRtk></AllProductsUsingRtk>}
            ></Route>

            {/* <Route
              path="create"
              element={<CreateProduct></CreateProduct>}
            ></Route> */}
            <Route
              path="create"
              element={<CreateProductUsingRtk></CreateProductUsingRtk>}
            ></Route>

            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route index element={<></>}></Route>
              {/* <Route
                path=":productId"
                element={<UpdateProduct></UpdateProduct>}
              ></Route> */}
              <Route
                path=":productId"
                element={<UpdateProductUsingRtk></UpdateProductUsingRtk>}
              ></Route>
            </Route>
            <Route
              path=":productId"
              element={<ProductDetail></ProductDetail>}
            ></Route>
          </Route>
        </Route>
      </Routes>
    </>
  );
};

export default Router;
