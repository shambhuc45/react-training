import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query";

export const reviewApi = createApi({
  reducerPath: "reviewApis",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:8000",
  }),
  tagTypes: [],
  endpoints: (builder) => ({
    readReviews: builder.query({
      query: () => {
        return {
          url: "/reviews",
          method: "GET",
        };
      },
    }),
    deleteReview: builder.mutation({
      query: (id) => {
        return {
          url: `/reviews/${id}`,
          method: "DELETE",
        };
      },
    }),
    readSpecificReview: builder.query({
      query: (id) => {
        return {
          url: `/reviews/${id}`,
          method: "GET",
        };
      },
    }),
    createReview: builder.mutation({
      query: (body) => {
        return {
          url: "/reviews",
          method: "POST",
          body: body,
        };
      },
    }),
  }),
});

export const {
  useReadReviewsQuery,
  useDeleteReviewMutation,
  useReadSpecificReviewQuery,
  useCreateReviewMutation,
} = reviewApi;
