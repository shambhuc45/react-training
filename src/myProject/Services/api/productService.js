import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const productApi = createApi({
  reducerPath: "productApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:8000",
  }),
  // firstly create tags
  tagTypes: ["readProducts", "readProductById"],
  endpoints: (builder) => ({
    readProducts: builder.query({
      query: () => {
        return {
          url: "/products",
          method: "GET",
        };
      },
      //secondly provide tags
      providesTags: ["readProducts"],
    }),
    readProductById: builder.query({
      query: (productId) => {
        return {
          url: `/products/${productId}`,
          method: "GET",
        };
      },
      providesTags: ["readProductById"],
    }),
    createProduct: builder.mutation({
      query: (body) => {
        return {
          url: "/products",
          method: "POST",
          body: body,
        };
      },
      invalidatesTags: ["readProducts"],
    }),
    updateProduct: builder.mutation({
      query: (data) => {
        console.log(data);
        return {
          url: `/products/${data.productId}`,
          method: "PATCH",
          body: data.body,
        };
      },
      invalidatesTags: ["readProducts", "readProductById"],
    }),
    deleteProduct: builder.mutation({
      query: (productId) => {
        return {
          url: `/products/${productId}`,
          method: "DELETE",
        };
      },
      //Lastly invalidatesTags
      invalidatesTags: ["readProducts"],
    }),
  }),
});

export const {
  useReadProductQuery,
  useReadProductByIdQuery,
  useCreateProductMutation,
  useDeleteProductMutation,
  useUpdateProductMutation,
} = productApi;

/* 
query and mutation

use query on "GET" method and use mutation on others.
   deleteProduct: builder.mutation({
      query: (productId) => {
        return {
          url: `/products/${productId}`,
          method: "DELETE",
        };
      },
    }),
*/

/* 
Generally we provides to query (get) and invalidateTags in mutation(other than get) */
