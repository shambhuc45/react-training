import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { hitApi } from "../Services/hitApi.js";
import { ToastContainer } from "react-toastify";
import showAlert from "../utils/showAlert.js";

const AllProducts = () => {
  // const MySwal = withReactContent(Swal);
  let [products, setProducts] = useState([]);
  let navigate = useNavigate();
  let fetchProducts = async () => {
    try {
      let response = await hitApi({
        url: "/products",
        method: "GET",
      });
      setProducts(response.data.data);
    } catch (error) {
      console.log(error.message);
    }
  };
  useEffect(() => {
    fetchProducts();
  }, []);

  // let showAlert = async (productId) => {
  //   try {
  //     let response = await MySwal.fire({
  //       title: "Confirmation",
  //       text: "Are you sure you want to delete?",
  //       icon: "question",
  //       showCancelButton: true,
  //       confirmButtonText: "Ok",
  //       cancelButtonText: "Cancel",
  //     });
  //     if (response.isConfirmed) {
  //       try {
  //         let response = await hitApi({
  //           url: `/products/${productId}`,
  //           method: "DELETE",
  //         });
  //         toast.success(response.data.message);
  //         fetchProducts();
  //       } catch (error) {
  //         console.log(error.message);
  //       }
  //     }
  //   } catch (error) {
  //     console.log(error.message);
  //   }
  // };

  let handleView = (product) => {
    return () => {
      navigate(`/products/${product._id}`);
    };
  };
  let handleEdit = (product) => {
    return () => {
      navigate(`/products/update/${product._id}`);
    };
  };

  return (
    <>
      <ToastContainer></ToastContainer>
      {products.map((product, i) => {
        return (
          <div
            key={product._id}
            style={{ border: "1px solid orange", paddingBottom: "10px" }}
          >
            <h1>Product: {product.name}</h1>
            <p>Price: ${product.price}</p>
            <p>Quantity: {product.quantity}</p>
            <img src={product.productImage} alt={product.name} height="200px" />
            <p>Desciption: {product.description}</p>
            {/* <button onClick={handleDelete(product)}>Delete Product</button> */}
            <button
              onClick={() => {
                showAlert(fetchProducts, `/products/${product._id}`);
              }}
            >
              Delete Product
            </button>
            <button onClick={handleView(product)}>View Product Details</button>
            <button onClick={handleEdit(product)}>Edit Product Details</button>
          </div>
        );
      })}
    </>
  );
};

export default AllProducts;
