import { Form, Formik } from "formik";
import React from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import FormikCheckbox from "../../myComponents/formik/FormikCheckbox.jsx";
import FormikInput from "../../myComponents/formik/FormikInput.jsx";
import FormikTextArea from "../../myComponents/formik/FormikTextArea.jsx";

const ProductForm = ({
  buttonName = "",
  handleSubmit = () => {},
  product = {},
  isLoading = false,
}) => {
  let initialValues = {
    name: product.name || "",
    description: product.description || "",
    price: product.price || 1,
    quantity: product.quantity || 1,
    featured: product.featured || false,
    productImage: product.productImage || "",
  };
  let validationSchema = yup.object({
    name: yup.string().required("Product Name is required"),
    description: yup.string().required("Description is required"),
    price: yup.number().required("Price is required"),
    quantity: yup.string().required("Quantity is required"),
    featured: yup.boolean(),
    productImage: yup.string().required("Product Image is required"),
  });

  return isLoading ? (
    <div>Loading...</div>
  ) : (
    <div>
      <ToastContainer></ToastContainer>

      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        enableReinitialize={true}
      >
        {(formik) => {
          return (
            <div>
              <Form>
                <FormikInput
                  name="name"
                  label="Name"
                  inputType="text"
                  required={true}
                ></FormikInput>
                <FormikTextArea
                  name="description"
                  label="Description"
                  required={true}
                ></FormikTextArea>

                <FormikInput
                  name="price"
                  inputType="number"
                  label="Price"
                  required={true}
                ></FormikInput>
                <FormikInput
                  name="quantity"
                  inputType="number"
                  label="Quantity"
                  required={true}
                ></FormikInput>
                <FormikCheckbox
                  name="featured"
                  label="Featured"
                ></FormikCheckbox>
                <FormikInput
                  name="productImage"
                  inputType="text"
                  label="Product Image"
                  required={true}
                ></FormikInput>

                <button type="submit">
                  {isLoading ? (
                    <div>{buttonName}...</div>
                  ) : (
                    <div>{buttonName}</div>
                  )}
                </button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </div>
  );
};

export default ProductForm;
