import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useReadProductByIdQuery } from "../Services/api/productService.js";

const ProductDetail = () => {
  let params = useParams();
  let productId = params.productId;

  const {
    isError: isErrorReadProductById,
    isLoading: isLoadingReadProductById,
    data: dataReadProductById,
    error: errorReadProductById,
  } = useReadProductByIdQuery(productId);

  const product = dataReadProductById?.result;

  useEffect(() => {
    if (isErrorReadProductById) {
      console.log(errorReadProductById?.error);
    }
  }, [isErrorReadProductById, errorReadProductById]);

  return isLoadingReadProductById ? (
    <div>Loading..</div>
  ) : (
    <>
      <h1>Product: {product?.name}</h1>
      <p>Price: ${product?.price}</p>
      <p>Quantity: {product?.quantity}</p>
      <img src={product?.productImage} alt={product?.name} height="200px" />
      <p>Desciption: {product?.description}</p>
      <p>Featured : {product?.featured ? "Yes" : "No"}</p>
    </>
  );
};

export default ProductDetail;
