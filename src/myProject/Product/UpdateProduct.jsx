import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import ProductForm from "./ProductForm.jsx";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { hitApi } from "../Services/hitApi.js";

const UpdateProduct = () => {
  let params = useParams();
  let productId = params.productId;
  let navigate = useNavigate();
  let [product, setProduct] = useState({});

  useEffect(() => {
    let fetchProduct = async () => {
      try {
        let response = await hitApi({
          url: `/products/${productId}`,
          method: "GET",
        });
        setProduct(response.data.result);
      } catch (error) {
        console.log(error.message);
      }
    };
    fetchProduct();
  }, [productId]);

  let handleSubmit = async (value, { resetForm }) => {
    let data = {
      ...value,
    };
    try {
      let response = await hitApi({
        url: `/products/${productId}`,
        method: "PATCH",
        data: data,
      });
      toast.success(response.data.message);
      setTimeout(() => {
        navigate(`/products/${productId}`);
      }, 1000);
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <ToastContainer></ToastContainer>
      <ProductForm
        buttonName="Update"
        handleSubmit={handleSubmit}
        product={product}
      ></ProductForm>
    </div>
  );
};

export default UpdateProduct;
