import React from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { hitApi } from "../Services/hitApi.js";
import ProductForm from "./ProductForm.jsx";

const CreateProduct = () => {
  let navigate = useNavigate();
  let handleSubmit = async (value, { resetForm }) => {
    let data = {
      ...value,
    };
    try {
      let response = await hitApi({
        url: "/products",
        method: "POST",
        data: data,
      });
      toast.success(response.data.message);
      setTimeout(() => {
        navigate("/products");
      }, 500);
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <ToastContainer></ToastContainer>
      <ProductForm
        buttonName="Create"
        handleSubmit={handleSubmit}
      ></ProductForm>
    </div>
  );
};

export default CreateProduct;
