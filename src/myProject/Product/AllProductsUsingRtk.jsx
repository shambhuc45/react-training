import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  useDeleteProductMutation,
  useReadProductQuery,
} from "../Services/api/productService.js";

const AllProductsUsingRtk = () => {
  let [deleteId, setDeleteId] = useState();

  const {
    isError: isErrorReadProduct,
    isLoading: isLoadingReadProduct,
    data: dataReadProduct,
    error: errorReadProduct,
  } = useReadProductQuery();

  const [
    deleteProduct,
    {
      isError: isErrorDeleteProduct,
      isLoading: isLoadingDeleteProduct,
      isSuccess: isSuccessDeleteProduct,
      data: dataDeleteProduct,
      error: errorDeleteProduct,
    },
  ] = useDeleteProductMutation();

  useEffect(() => {
    if (isErrorReadProduct) {
      console.log(errorReadProduct?.error);
    }
  }, [isErrorReadProduct, errorReadProduct]);

  useEffect(() => {
    if (isErrorDeleteProduct) {
      console.log(errorDeleteProduct?.error);
    }
  }, [isErrorDeleteProduct, errorDeleteProduct]);

  useEffect(() => {
    if (isSuccessDeleteProduct) {
      toast.success(dataDeleteProduct?.message);
    }
  }, [isSuccessDeleteProduct, dataDeleteProduct]);

  let products = dataReadProduct?.data;
  let navigate = useNavigate();

  return isLoadingReadProduct ? (
    <div>
      <ToastContainer />
      Loading...
    </div>
  ) : (
    <>
      <ToastContainer />

      {products?.map((product, i) => {
        return (
          <div
            key={product._id}
            style={{ border: "1px solid orange", paddingBottom: "10px" }}
          >
            <h1>Product: {product.name}</h1>
            <p>Price: ${product.price}</p>
            <p>Quantity: {product.quantity}</p>
            <img src={product.productImage} alt={product.name} height="200px" />
            <p>Desciption: {product.description}</p>
            <button
              onClick={() => {
                deleteProduct(product._id);
                setDeleteId(product._id);
              }}
            >
              {isLoadingDeleteProduct && deleteId === product._id
                ? "Deleting.."
                : "Delete Product"}
            </button>
            <button
              onClick={() => {
                navigate(`/products/${product._id}`);
              }}
            >
              View Product Details
            </button>
            <button
              onClick={() => {
                navigate(`/products/update/${product._id}`);
              }}
            >
              Edit Product Details
            </button>
          </div>
        );
      })}
    </>
  );
};

export default AllProductsUsingRtk;
