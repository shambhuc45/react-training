import React, { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  useReadProductByIdQuery,
  useUpdateProductMutation,
} from "../Services/api/productService.js";
import ProductForm from "./ProductForm.jsx";

const UpdateProductUsingRtk = () => {
  let params = useParams();
  let productId = params.productId;
  let navigate = useNavigate();

  const {
    data: productData,
    isLoading: isLoadingProductData,
    error: errorProductData,
  } = useReadProductByIdQuery(productId);

  const [
    updateProduct,
    {
      isError: isErrorUpdateProduct,
      isLoading: isLoadingUpdateProduct,
      isSuccess: isSuccessUpdateProduct,
      data: dataUpdateProduct,
      error: errorUpdateProduct,
    },
  ] = useUpdateProductMutation();

  useEffect(() => {
    if (isErrorUpdateProduct) {
      toast.error(errorUpdateProduct?.error);
    } else if (isSuccessUpdateProduct) {
      toast.success(dataUpdateProduct?.message);
      navigate("/products");
    }
  }, [
    isErrorUpdateProduct,
    isSuccessUpdateProduct,
    errorUpdateProduct,
    dataUpdateProduct,
    navigate,
  ]);

  let handleSubmit = async (value, { resetForm }) => {
    let data = {
      ...value,
    };
    updateProduct({ productId: productId, body: data });
  };

  if (isLoadingProductData) {
    return <div>Loading...</div>;
  }

  if (errorProductData) {
    return <div>Error loading product data</div>;
  }

  return (
    <div>
      <ToastContainer />
      <ProductForm
        buttonName="Update"
        handleSubmit={handleSubmit}
        product={productData?.result}
        isLoading={isLoadingUpdateProduct}
      />
    </div>
  );
};

export default UpdateProductUsingRtk;
