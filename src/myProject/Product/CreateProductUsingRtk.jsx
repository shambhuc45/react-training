import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { useCreateProductMutation } from "../Services/api/productService.js";
import ProductForm from "./ProductForm.jsx";

const CreateProductUsingRtk = () => {
  let navigate = useNavigate();
  const [
    createProduct,
    {
      isError: isErrorCreateProduct,
      isLoading: isLoadingCreateProduct,
      isSuccess: isSuccessCreateProduct,
      data: dataCreateProduct,
      error: errorCreateProduct,
    },
  ] = useCreateProductMutation();

  useEffect(() => {
    if (isErrorCreateProduct) {
      toast.error(errorCreateProduct?.error);
    } else if (isSuccessCreateProduct) {
      toast.success(dataCreateProduct.message);
      navigate("/products");
    }
  }, [
    isErrorCreateProduct,
    errorCreateProduct,
    isSuccessCreateProduct,
    dataCreateProduct,
    navigate,
  ]);

  let handleSubmit = async (value, { resetForm }) => {
    let data = {
      ...value,
    };
    createProduct(data);
    toast.success(dataCreateProduct?.message);
  };

  return (
    <div>
      <ProductForm
        buttonName="Create"
        handleSubmit={handleSubmit}
        isLoading={isLoadingCreateProduct}
      ></ProductForm>
    </div>
  );
};

export default CreateProductUsingRtk;
