import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { hitApi } from "../../myProject/Services/hitApi.js";

const AdminProfile = () => {
  const [profile, setProfile] = useState({});
  const navigate = useNavigate();
  let getAdminProfile = async () => {
    let token = localStorage.getItem("token");
    try {
      let response = await hitApi({
        url: "/web-users/my-profile",
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setProfile(response.data.result);
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };
  useEffect(() => {
    getAdminProfile();
  }, []);
  return (
    <>
      <ToastContainer></ToastContainer>
      <h3>My Profile:</h3>
      <p>Full Name: {profile.fullName}</p>
      <p>Email: {profile.email}</p>
      <p>Gender: {profile.gender}</p>
      <p>Phone Number: {profile.phoneNumber}</p>
      <p>Role: {profile.role}</p>
      <p>Date of Birth: {new Date(profile.dob).toLocaleDateString()}</p>

      <button
        onClick={() => {
          navigate("/admin/profile-update");
        }}
      >
        Edit Profile
      </button>
    </>
  );
};

export default AdminProfile;
