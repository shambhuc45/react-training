import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { hitApi } from "../../myProject/Services/hitApi.js";

const ReadSpecificUser = () => {
  const [user, setUser] = useState({});
  const navigate = useNavigate();
  let params = useParams();
  let id = params._id;
  let getUserProfile = async () => {
    let token = localStorage.getItem("token");
    try {
      let response = await hitApi({
        url: `/web-users/${id}`,
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setUser(response.data.result);
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };
  useEffect(() => {
    getUserProfile();
  }, []);
  return (
    <>
      <ToastContainer></ToastContainer>
      <h3>User Profile:</h3>
      <p>Full Name: {user.fullName}</p>
      <p>Email: {user.email}</p>
      <p>Gender: {user.gender}</p>
      <p>Phone Number: {user.phoneNumber}</p>
      <p>Role: {user.role}</p>
      <p>Date of Birth: {new Date(user.dob).toLocaleDateString()}</p>

      <button
        onClick={() => {
          navigate(`/admin/update/${user._id}`);
        }}
      >
        Edit User
      </button>
    </>
  );
};

export default ReadSpecificUser;
