import React, { useContext } from "react";
import FormikInput from "../formik/FormikInput";
import { Form, Formik } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import { GlobalVariableContext } from "../../MyApp.jsx";
import { hitApi } from "../../myProject/Services/hitApi.js";

const AdminLogin = () => {
  let initialValues = {
    email: "",
    password: "",
  };
  let { setIsLoggedIn } = useContext(GlobalVariableContext);
  let navigate = useNavigate();
  let handleSubmit = async (value, other) => {
    let data = {
      ...value,
    };
    try {
      let response = await hitApi({
        url: "/web-users/login",
        method: "POST",
        data: data,
      });
      let token = response.data.token;
      localStorage.setItem("token", token);
      setIsLoggedIn(token);
      navigate("/admin");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };
  let validateSchema = yup.object({
    email: yup
      .string()
      .required("Email is required")
      .matches(
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
        "Please Enter a valid Email."
      ),
    password: yup
      .string()
      .required("Password is required")
      .min(8, "Password must be at least 8 characters long")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&*!])[A-Za-z\d@#$%^&*!]+$/,
        "Password must contain at least 1 uppercase, 1 lowercase, one digit and one special characters."
      ),
  });
  return (
    <>
      <ToastContainer></ToastContainer>
      <h3>Login Form:</h3>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validateSchema}
      >
        {(formik) => {
          return (
            <div>
              <Form>
                <FormikInput
                  name="email"
                  label="Email"
                  inputType="email"
                  required={true}
                  placeholder="e.g: abc@gmail.com"
                ></FormikInput>
                <FormikInput
                  name="password"
                  label="Password"
                  inputType="password"
                  required={true}
                  placeholder="e.g. Password@123"
                ></FormikInput>
                <button type="submit">Login</button>
                <button
                  type="button"
                  onClick={() => {
                    navigate("/admin/forgot-password");
                  }}
                >
                  Forgot Password?
                </button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </>
  );
};

export default AdminLogin;
