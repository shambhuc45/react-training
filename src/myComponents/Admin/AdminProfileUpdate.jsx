import { Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import FormikInput from "../formik/FormikInput";
import FormikRadio from "../formik/FormikRadio";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";
import { hitApi } from "../../myProject/Services/hitApi.js";

const AdminProfileUpdate = () => {
  let token = localStorage.getItem("token");
  let navigate = useNavigate();
  const [userData, setUserData] = useState({});
  let fetchData = async () => {
    try {
      let response = await hitApi({
        url: "/web-users/my-profile",
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setUserData(response.data.result);
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const initialValues = {
    fullName: userData.fullName || "",
    phoneNumber: userData.phoneNumber || "",
    gender: userData.gender || "male",
    dob: new Date(userData.dob).toLocaleDateString("en-CA") || "",
  };
  let handleSubmit = async (value) => {
    let data = {
      ...value,
    };
    try {
      let response = await hitApi({
        url: "/web-users/update-profile",
        method: "PATCH",
        data: data,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      console.log(response);
      navigate("/admin/my-profile");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };
  let genderOptions = [
    {
      label: "Male",
      value: "male",
    },
    {
      label: "Female",
      value: "female",
    },
    {
      label: "Other",
      value: "other",
    },
  ];
  let validateSchema = yup.object({
    fullName: yup
      .string()
      .required("Full Name is required")
      .min(5, "Full Name must be of at least 5 Characters.")
      .max(20, "Full Name cannot exceed 20 characters")
      .matches(
        /^[a-zA-Z]+(?:\s[a-zA-Z]+)?(?:\s[a-zA-Z]+)?$/,
        "Please Enter Your valid Name"
      ),
    phoneNumber: yup
      .string()
      .required("Phone Number is required.")
      .matches(
        /^\+?\d{8,15}$/,
        "Please enter a valid phone number. It should be between 8 to 15 digits long and can optionally start with a '+' sign"
      ),
    gender: yup.string().required("Please select your gender."),
    dob: yup.date().required("Date of Birth is required."),
  });
  return (
    <>
      <ToastContainer></ToastContainer>
      <h3>Admin Update Form:</h3>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validateSchema}
        enableReinitialize={true}
      >
        {(formik) => {
          return (
            <div>
              <Form>
                <FormikInput
                  name="fullName"
                  label="Full Name"
                  inputType="text"
                  placeholder="e.g: Tom Cruise"
                ></FormikInput>

                <FormikInput
                  name="phoneNumber"
                  label="Phone Number"
                  inputType="number"
                ></FormikInput>
                <FormikRadio
                  name="gender"
                  label="Gender"
                  options={genderOptions}
                ></FormikRadio>
                <FormikInput
                  name="dob"
                  label="Date of Birth"
                  inputType="date"
                ></FormikInput>
                <button type="submit">Update</button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </>
  );
};

export default AdminProfileUpdate;
