import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { hitApi } from "../../myProject/Services/hitApi.js";

const ReadAllUsers = () => {
  const [users, setUsers] = useState([]);
  const navigate = useNavigate();
  let getAllUsers = async () => {
    let token = localStorage.getItem("token");
    try {
      let response = await hitApi({
        url: "/web-users",
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setUsers(response.data.result);
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };
  useEffect(() => {
    getAllUsers();
  }, []);

  return (
    <>
      <ToastContainer></ToastContainer>
      <h3>All Users:</h3>
      {users.map((user, i) => {
        return (
          <div
            key={user._id}
            style={{ border: "1px solid orange", padding: "10px" }}
          >
            <h4>User Name : {user.fullName}</h4>
            <p>Email : {user.email}</p>
            <p>Gender : {user.gender}</p>
            <p>Phone : {user.phoneNumber}</p>
            <button
              onClick={() => {
                navigate(`/admin/${user._id}`);
              }}
            >
              View
            </button>
            &nbsp;&nbsp;&nbsp;
            <button
              onClick={() => {
                navigate(`/admin/update/${user._id}`);
              }}
            >
              Edit
            </button>
            &nbsp;&nbsp;&nbsp;
            <button
              onClick={async () => {
                await hitApi({
                  url: `/web-users/${user._id}`,
                  method: "DELETE",
                  headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                  },
                });
                getAllUsers();
              }}
            >
              Delete
            </button>
            &nbsp;&nbsp;&nbsp;
          </div>
        );
      })}
    </>
  );
};

export default ReadAllUsers;
