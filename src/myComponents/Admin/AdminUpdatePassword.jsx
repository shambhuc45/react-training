import React, { useContext } from "react";
import FormikInput from "../formik/FormikInput";
import { Form, Formik } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import { hitApi } from "../../myProject/Services/hitApi.js";
import { GlobalVariableContext } from "../../MyApp.jsx";

const AdminUpdatePassword = () => {
  let { setIsLoggedIn } = useContext(GlobalVariableContext);
  let initialValues = {
    oldPassword: "",
    newPassword: "",
    confirmPassword: "",
  };
  let navigate = useNavigate();
  let token = localStorage.getItem("token");
  let handleSubmit = async (value, other) => {
    let data = {
      ...value,
    };
    try {
      await hitApi({
        url: "/web-users/update-password",
        method: "PATCH",
        data: data,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      localStorage.removeItem("token");
      setIsLoggedIn(false);
      navigate("/admin/login");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };
  let validateSchema = yup.object({
    oldPassword: yup.string().required("Old Password is required"),
    newPassword: yup
      .string()
      .required("Password is required")
      .min(8, "Password must be at least 8 characters long")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&*!])[A-Za-z\d@#$%^&*!]+$/,
        "Password must contain at least 1 uppercase, 1 lowercase, one digit and one special characters."
      ),
    confirmPassword: yup
      .string()
      .required("Confirm Password is required")
      .oneOf([yup.ref("newPassword"), null], "Password didnot match."),
  });
  return (
    <>
      <ToastContainer></ToastContainer>
      <h3>Change Password:</h3>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validateSchema}
      >
        {(formik) => {
          return (
            <div>
              <Form>
                <FormikInput
                  name="oldPassword"
                  label="Old Password"
                  inputType="password"
                  required={true}
                ></FormikInput>
                <FormikInput
                  name="newPassword"
                  label="New Password"
                  inputType="password"
                  required={true}
                ></FormikInput>
                <FormikInput
                  name="confirmPassword"
                  label="Confirm Password"
                  inputType="password"
                  required={true}
                />

                <button type="submit">Change</button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </>
  );
};

export default AdminUpdatePassword;
