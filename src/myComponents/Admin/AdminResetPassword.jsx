import { Form, Formik } from "formik";
import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import FormikInput from "../formik/FormikInput";
import { hitApi } from "../../myProject/Services/hitApi.js";

const AdminResetPassword = () => {
  let initialValues = {
    password: "",
    confirmPassword: "",
  };
  let navigate = useNavigate();
  let [query] = useSearchParams();
  let token = query.get("token");

  let handleSubmit = async (value, other) => {
    let data = {
      ...value,
    };
    try {
      let response = await hitApi({
        url: "/web-users/reset-password",
        method: "PATCH",
        data: data,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      toast.success(response.data.message);
      navigate("/admin/login");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  let validateSchema = yup.object({
    password: yup
      .string()
      .required("Password is required")
      .min(8, "Password must be at least 8 characters long")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&*!])[A-Za-z\d@#$%^&*!]+$/,
        "Password must contain at least 1 uppercase, 1 lowercase, one digit and one special characters."
      ),
    confirmPassword: yup
      .string()
      .required("Confirm Password is required")
      .oneOf([yup.ref("password"), null], "Password didnot match."),
  });
  return (
    <>
      <ToastContainer></ToastContainer>
      <h3>Create New Password:</h3>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validateSchema}
      >
        {(formik) => {
          return (
            <div>
              <Form>
                <FormikInput
                  name="password"
                  label="New Password"
                  inputType="password"
                  required={true}
                ></FormikInput>
                <FormikInput
                  name="confirmPassword"
                  label="Confirm Password"
                  inputType="password"
                  required={true}
                />

                <button type="submit">Reset</button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </>
  );
};

export default AdminResetPassword;
