import React from "react";
import FormikInput from "../formik/FormikInput.jsx";
import { Form, Formik } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import { hitApi } from "../../myProject/Services/hitApi.js";

const AdminForgotPassword = () => {
  let initialValues = {
    email: "",
  };
  let handleSubmit = async (value, { resetForm }) => {
    let data = {
      ...value,
    };
    try {
      let response = await hitApi({
        url: "/web-users/forgot-password",
        method: "POST",
        data: data,
      });
      toast.success(response.data.message);
      resetForm();
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };
  let validateSchema = yup.object({
    email: yup
      .string()
      .required("Email is required")
      .matches(
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
        "Please Enter a valid Email."
      ),
  });
  return (
    <>
      <ToastContainer></ToastContainer>
      <h3>Forgot Password:</h3>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validateSchema}
      >
        {(formik) => {
          return (
            <div>
              <Form>
                <FormikInput
                  name="email"
                  label="Email"
                  inputType="email"
                  required={true}
                  placeholder="e.g: abc@gmail.com"
                ></FormikInput>
                <button type="submit">Submit</button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </>
  );
};

export default AdminForgotPassword;
