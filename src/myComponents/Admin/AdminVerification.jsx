import React, { useEffect } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { hitApi } from "../../myProject/Services/hitApi.js";

const AdminVerification = () => {
  let navigate = useNavigate();
  let [query] = useSearchParams();
  let token = query.get("token");

  let verifyEmail = async () => {
    try {
      let response = await hitApi({
        url: "/web-users/verify-email",
        method: "PATCH",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      toast.success(response.data.message);
      setTimeout(() => {
        navigate("/admin/login");
      }, 1000);
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };
  useEffect(() => {
    verifyEmail();
  }, []);
  return (
    <div>
      <ToastContainer></ToastContainer>
      <h4>Redirecting</h4>
    </div>
  );
};

export default AdminVerification;
