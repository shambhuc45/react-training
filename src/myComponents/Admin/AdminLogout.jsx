import React, { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import { GlobalVariableContext } from "../../MyApp.jsx";

const AdminLogout = () => {
  let navigate = useNavigate();
  let { setIsLoggedIn } = useContext(GlobalVariableContext);
  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      localStorage.removeItem("token");
      setIsLoggedIn(null);
      toast.success("Logged out Successfully.");
    }

    navigate("/");
  }, []);
  return (
    <>
      <ToastContainer></ToastContainer>
    </>
  );
};

export default AdminLogout;
