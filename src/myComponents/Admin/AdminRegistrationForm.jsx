import { Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "../formik/FormikInput";
import FormikRadio from "../formik/FormikRadio";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { hitApi } from "../../myProject/Services/hitApi.js";

const AdminRegistrationForm = () => {
  let initialValues = {
    fullName: "",
    email: "",
    password: "",
    phoneNumber: "",
    gender: "male",
    dob: "",
  };
  let handleSubmit = async (value, { resetForm }) => {
    let data = {
      ...value,
      role: "admin",
    };
    try {
      let response = await hitApi({
        url: "/web-users",
        method: "POST",
        data: data,
      });
      toast.success(response.data.message);
      resetForm();
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };
  let genderOptions = [
    {
      label: "Male",
      value: "male",
    },
    {
      label: "Female",
      value: "female",
    },
    {
      label: "Other",
      value: "other",
    },
  ];
  let validateSchema = yup.object({
    fullName: yup
      .string()
      .required("Full Name is required")
      .min(5, "Full Name must be of at least 5 Character long.")
      .max(25, "Full Name cannot exceed 25 characters")
      .matches(
        /^[a-zA-Z]+(?:\s[a-zA-Z]+)?(?:\s[a-zA-Z]+)?$/,
        "Please Enter Your valid Name"
      ),
    email: yup
      .string()
      .required("Email is required")
      .matches(
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
        "Please Enter a valid Email."
      ),
    password: yup
      .string()
      .required("Password is required")
      .min(8, "Password must be at least 8 characters long")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&*!])[A-Za-z\d@#$%^&*!]+$/,
        "Password must contain at least 1 uppercase, 1 lowercase, one digit and one special characters."
      ),
    phoneNumber: yup
      .string()
      .required("Phone Number is required.")
      .matches(
        /^\+?\d{8,15}$/,
        "Please enter a valid phone number. It should be between 8 to 15 digits long and can optionally start with a '+' sign"
      ),
    gender: yup.string().required("Please select your gender."),
    dob: yup.date().required("Date of Birth is required."),
  });
  return (
    <>
      <ToastContainer></ToastContainer>
      <h3>Admin Registration Form:</h3>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validateSchema}
      >
        {(formik) => {
          return (
            <div>
              <Form>
                <FormikInput
                  name="fullName"
                  label="Full Name"
                  inputType="text"
                  required={true}
                  placeholder="e.g: Tom Cruise"
                ></FormikInput>
                <FormikInput
                  name="email"
                  label="Email"
                  inputType="email"
                  required={true}
                  placeholder="e.g: abc@gmail.com"
                ></FormikInput>
                <FormikInput
                  name="password"
                  label="Password"
                  inputType="password"
                  required={true}
                  placeholder="e.g. Password@123"
                ></FormikInput>
                <FormikInput
                  name="phoneNumber"
                  label="Phone Number"
                  inputType="number"
                  required={true}
                ></FormikInput>
                <FormikRadio
                  name="gender"
                  label="Gender"
                  options={genderOptions}
                ></FormikRadio>
                <FormikInput
                  name="dob"
                  label="Date of Birth"
                  inputType="date"
                  required={true}
                ></FormikInput>
                <button type="submit">Register</button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </>
  );
};

export default AdminRegistrationForm;
