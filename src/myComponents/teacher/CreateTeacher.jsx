import React, {  useState } from "react";
import { hitApi } from "../../myProject/Services/hitApi.js";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ReactDropZone from "../formik/ReactDropZone.jsx";

const CreateTeacher = () => {
  const [fullName, setFullName] = useState("");
  const [age, setAge] = useState(0);
  const [isMarried, setIsMarried] = useState(false);
  const [subject, setSubject] = useState("");
  const [profileImageLink, setProfileImageLink] = useState("");


  const handleSubmit = async (e) => {
    e.preventDefault();

    let data = {
      fullName: fullName,
      age: age,
      isMarried: isMarried,
      subject: subject,
      profileImage: profileImageLink,
    };

    try {
      let result = await hitApi({
        url: "/teachers",
        method: "post",
        data: data,
      });
      console.log(result);
      toast.success(result.data.message);
    } catch (error) {
      console.log(error.message);
    }
  };
  return (
    <>
      <ToastContainer></ToastContainer>
      <form action="" onSubmit={handleSubmit}>
        <div style={{ margin: "5px" }}>
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            id="name"
            value={fullName}
            onChange={(e) => {
              setFullName(e.target.value);
            }}
          />
        </div>
        <div style={{ margin: "5px" }}>
          <label htmlFor="age">Age:</label>
          <input
            type="number"
            id="age"
            value={age}
            onChange={(e) => {
              setAge(e.target.value);
            }}
          />
        </div>
        <div style={{ margin: "5px" }}>
          <input
            type="checkbox"
            id="isMarried"
            checked={isMarried}
            onChange={(e) => {
              setIsMarried(e.target.checked);
            }}
          />
          <label htmlFor="isMarried">Is Married?</label>
        </div>
        <div style={{ margin: "5px" }}>
          <label htmlFor="subject">Subject:</label>
          <input
            type="text"
            id="subject"
            value={subject}
            onChange={(e) => {
              setSubject(e.target.value);
            }}
          />
        </div>
        <ReactDropZone profileImageLink={profileImageLink}  setProfileImageLink={setProfileImageLink}></ReactDropZone>
        
        <button type="submit">Create</button>
      </form>
    </>
  );
};

export default CreateTeacher;

//name age isMarried subject
