import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { GlobalVariableContext } from "../MyApp.jsx";

const MyNavbar = () => {
  let { isLoggedIn } = useContext(GlobalVariableContext);

  return (
    <>
      <NavLink
        to="/teachers"
        style={{ marginRight: "15px", textDecoration: "none" }}
      >
        Teachers
      </NavLink>
      <NavLink
        to="/teachers/create"
        style={{ marginRight: "15px", textDecoration: "none" }}
      >
        Create Teacher
      </NavLink>

      {isLoggedIn ? (
        <>
          <NavLink
            to="/admin/my-profile"
            style={{ marginRight: "15px", textDecoration: "none" }}
          >
            My Profile
          </NavLink>
          <NavLink
            to="/admin/read-all-users"
            style={{ marginRight: "15px", textDecoration: "none" }}
          >
            Read All Users
          </NavLink>

          <NavLink
            to="/admin/update-password"
            style={{ marginRight: "15px", textDecoration: "none" }}
          >
            Change Password
          </NavLink>
          <NavLink
            to="/admin/logout"
            style={{ marginRight: "15px", textDecoration: "none" }}
          >
            Log Out
          </NavLink>
        </>
      ) : (
        <>
          <NavLink
            to="/admin/login"
            style={{ marginRight: "15px", textDecoration: "none" }}
          >
            Admin Login
          </NavLink>
          <NavLink
            to="/admin/register"
            style={{ marginRight: "15px", textDecoration: "none" }}
          >
            Admin Register
          </NavLink>
        </>
      )}
    </>
  );
};

export default MyNavbar;
