import React, { useContext } from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import CreateTeacher from "../teacher/CreateTeacher";
import AllTeachers from "../teacher/AllTeachers";
import MyNavbar from "../MyNavbar";
import AdminRegistrationForm from "../Admin/AdminRegistrationForm";
import AdminVerification from "../Admin/AdminVerification";
import AdminLogin from "../Admin/AdminLogin";
import AdminProfile from "../Admin/AdminProfile";
import AdminLogout from "../Admin/AdminLogout";
import AdminProfileUpdate from "../Admin/AdminProfileUpdate";
import AdminUpdatePassword from "../Admin/AdminUpdatePassword";
import AdminForgotPassword from "../Admin/AdminForgotPassword";
import AdminResetPassword from "../Admin/AdminResetPassword";
import ReadAllUsers from "../Admin/ReadAllUsers";
import ReadSpecificUser from "../Admin/ReadSpecificUser";
import UpdateSpecificUser from "../Admin/UpdateSpecificUser.jsx";
import { GlobalVariableContext } from "../../MyApp.jsx";

const MyRoutes = () => {
  let { isLoggedIn } = useContext(GlobalVariableContext);
  return (
    <>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <MyNavbar></MyNavbar>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route index element={<div>This is Landing Page</div>}></Route>

          <Route
            path="verify-email"
            element={<AdminVerification></AdminVerification>}
          ></Route>
          <Route
            path="teachers"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<AllTeachers />}></Route>
            <Route path="create" element={<CreateTeacher />}></Route>
          </Route>
          <Route
            path="admin"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            {isLoggedIn ? (
              <>
                <Route
                  index
                  element={<div>This is admin Dashboard</div>}
                ></Route>
                <Route
                  path="my-profile"
                  element={<AdminProfile></AdminProfile>}
                ></Route>
                <Route
                  path="logout"
                  element={<AdminLogout></AdminLogout>}
                ></Route>
                <Route
                  path="profile-update"
                  element={<AdminProfileUpdate></AdminProfileUpdate>}
                ></Route>
                <Route
                  path="update-password"
                  element={<AdminUpdatePassword></AdminUpdatePassword>}
                ></Route>
                <Route
                  path="reset-password"
                  element={<AdminResetPassword></AdminResetPassword>}
                ></Route>
                <Route
                  path="read-all-users"
                  element={<ReadAllUsers></ReadAllUsers>}
                ></Route>
                <Route
                  path=":_id"
                  element={<ReadSpecificUser></ReadSpecificUser>}
                ></Route>
                <Route
                  path="update"
                  element={
                    <div>
                      <Outlet></Outlet>
                    </div>
                  }
                >
                  <Route
                    index
                    element={<div>This is Update Dashboard</div>}
                  ></Route>

                  <Route
                    path=":_id"
                    element={<UpdateSpecificUser></UpdateSpecificUser>}
                  ></Route>
                </Route>
              </>
            ) : (
              <>
                <Route path="login" element={<AdminLogin></AdminLogin>}></Route>

                <Route
                  path="register"
                  element={<AdminRegistrationForm></AdminRegistrationForm>}
                ></Route>
                <Route
                  path="forgot-password"
                  element={<AdminForgotPassword></AdminForgotPassword>}
                ></Route>
                <Route path="*" element={<>Page Not Found</>}></Route>
              </>
            )}
          </Route>
        </Route>
      </Routes>
    </>
  );
};

export default MyRoutes;
