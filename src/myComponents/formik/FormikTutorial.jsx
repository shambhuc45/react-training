import { Formik, Form } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "./FormikInput";
import FormikRadio from "./FormikRadio";
import FormikSelect from "./FormikSelect";
import FormikTextArea from "./FormikTextArea";
import FormikCheckbox from "./FormikCheckbox";

const FormikTutorial = () => {
  let initialValues = {
    fullName: "",
    email: "",
    password: "",
    gender: "male",
    country: "",
    isMarried: false,
    description: "",
    phoneNumber: "",
    age: 0,
  };
  let onSubmit = (value, other) => {
    console.log(value);
  };
  let validationSchema = yup.object({
    fullName: yup
      .string()
      .required("Full Name is required")
      .min(5, "Full Name must be of at least 5 Characters.")
      .max(20, "Full Name cannot exceed 20 characters")
      .matches(
        /^[a-zA-Z]+(?:\s[a-zA-Z]+)?(?:\s[a-zA-Z]+)?$/,
        "Please Enter Your valid Name"
      ),
    email: yup
      .string()
      .required("Email is required")
      .matches(
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
        "Please Enter a valid Email."
      ),
    password: yup
      .string()
      .required("Password is required")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
        "Password must be at least 8 characters long and contain at least one uppercase letter, one lowercase letter, one digit, and one special character."
      ),
    gender: yup.string().required("Gender is required"),
    country: yup.string().required("Country is required"),
    isMarried: yup.boolean(),
    description: yup.string(),
    phoneNumber: yup
      .string()
      .required("Phone number is required")
      .min(10, "Phone Number must be of 10 digits")
      .max(10, "Phone Number must be of 10 digits"),
    //   .matches(
    //     /^\+?\d{8,15}$/,
    //     "Please enter a valid phone number. It should be between 8 to 15 digits long and can optionally start with a '+' sign"
    //   ),
    age: yup.number().min(18, "Age must be 18"),
  });

  let genderOptions = [
    {
      label: "Male",
      value: "male",
    },
    {
      label: "Female",
      value: "female",
    },
    {
      label: "Other",
      value: "other",
    },
  ];
  let countryOptions = [
    { label: "Select Country", value: "", disabled: true },
    { label: "China", value: "china" },
    { label: "India", value: "india" },
    { label: "Bhutan", value: "bhutan" },
    { label: "Nepal", value: "nepal" },
  ];
  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <div>
              <Form>
                <FormikInput
                  name="fullName"
                  label="Full name"
                  inputType="text"
                  required={true}
                ></FormikInput>
                <FormikInput
                  name="email"
                  label="Email"
                  inputType="email"
                  required={true}
                ></FormikInput>

                <FormikInput
                  name="password"
                  inputType="password"
                  label="Password"
                  required={true}
                  placeholder="e.g. Password@123"
                ></FormikInput>

                <FormikRadio
                  name="gender"
                  label="Gender"
                  options={genderOptions}
                ></FormikRadio>

                <FormikSelect
                  name="country"
                  label="Country"
                  required={true}
                  options={countryOptions}
                ></FormikSelect>

                <FormikCheckbox
                  name="isMarried"
                  label="Is Married"
                ></FormikCheckbox>

                <FormikTextArea
                  name="description"
                  label="Description"
                ></FormikTextArea>

                <FormikInput
                  name="phoneNumber"
                  inputType="text"
                  label="Phone Number"
                  required={true}
                ></FormikInput>

                <FormikInput
                  name="age"
                  inputType="number"
                  label="Age"
                  required={true}
                ></FormikInput>
                <button type="submit">Submit</button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </div>
  );
};

export default FormikTutorial;
