import { Field } from "formik";
import React from "react";

const FormikInput = ({
  name,
  label,
  inputType,
  onChange,
  required,
  disabled,
  ...props
}) => {
  return (
    <>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <div style={{ marginTop: "5px" }}>
              <label htmlFor={name}>
                {label}{" "}
                {required ? <span style={{ color: "red" }}>*</span> : null}:
              </label>
              <input
                {...field}
                {...props}
                disabled={disabled}
                type={inputType}
                id={name}
                value={meta.value}
                onChange={onChange ? onChange : field.onChange}
                // onChange={(e) => {
                //   formik.setFieldValue("fullName", e.target.value);
                // }}
              />
              {meta.error && meta.touched ? (
                <div style={{ color: "red" }}>{meta.error}</div>
              ) : null}
            </div>
          );
        }}
      </Field>

      {/* 
      <Field name="age">
                  {({ field, form, meta }) => {
                    return (
                      <div style={{ marginTop: "5px" }}>
                        <label htmlFor="age">Age:</label>
                        <input
                          {...field}
                          type="number"
                          id="age"
                          value={meta.value}
                          onChange={(e) => {
                            formik.setFieldValue("age", e.target.value);
                          }}
                        />
                      </div>
                    );
                  }}
                </Field> */}
    </>
  );
};

export default FormikInput;
