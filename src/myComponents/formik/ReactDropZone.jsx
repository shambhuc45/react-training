import React, { useCallback } from "react";
import { hitApi } from "../../myProject/Services/hitApi.js";
import { useDropzone } from "react-dropzone";
import { ToastContainer, toast } from "react-toastify";

const ReactDropZone = ({ setProfileImageLink, profileImageLink }) => {
  const onDrop = useCallback(async (acceptedFiles) => {
    let formData = new FormData();
    formData.append("file1", acceptedFiles[0]);

    try {
      let response = await hitApi({
        url: "/files/single",
        method: "POST",
        data: formData,
      });
      toast.success(response.data.message);
      setProfileImageLink(response.data.result);
    } catch (error) {
      console.log(error.message);
    }
  }, []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <>
      <ToastContainer></ToastContainer>
      <div
        {...getRootProps()}
        style={{ border: "1px dotted black", width: "50%" }}
      >
        <input {...getInputProps()} />
        {isDragActive ? (
          <p>Drop the files here ...</p>
        ) : (
          <p>Drag 'n' drop some files here, or click to select files</p>
        )}
        {profileImageLink ? (
          <img src={profileImageLink} alt="profileImage" height="150px" />
        ) : null}
      </div>
    </>
  );
};

export default ReactDropZone;
