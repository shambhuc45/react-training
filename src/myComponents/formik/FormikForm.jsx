import { Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "./FormikInput";
import FormikTextArea from "./FormikTextArea";
import FormikSelect from "./FormikSelect";
import FormikRadio from "./FormikRadio";
import FormikCheckbox from "./FormikCheckbox";

const FormikForm = () => {
  let initialValues = {
    fullName: "",
    age: 0,
    isMarried: false,
    subject: "",
    description: "",
    country: "",
    gender: "male",
  };
  let handleSubmit = (value, other) => {
    console.log(value);
    // value = {
    //   fullName: "",
    //   age: 0,
    //   isMarried: false,
    //   subject: "",
    // };
  };
  let validationSchema = yup.object({
    fullName: yup.string().required("Full Name is required."),
    age: yup.number().required("Age is required."),
    subject: yup.string().required("Subject is required."),
    description: yup.string().required("Description is required."),
    country: yup.string().required("Please select your Country."),
    gender: yup.string().required("Please select your Gender."),
    isMarried: yup.boolean(),
  });
  let countries = [
    { label: "Select Country", value: "", disabled: true },
    { label: "China", value: "china" },
    { label: "India", value: "india" },
    { label: "Bhutan", value: "bhutan" },
    { label: "Nepal", value: "nepal" },
  ];
  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Others", value: "others" },
  ];
  return (
    <>
      <h1>Form using Formik:</h1>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        enableReinitialize={true}
      >
        {(formik) => {
          return (
            <div>
              <Form>
                <FormikInput
                  name="fullName"
                  label="Full Name"
                  inputType="text"
                  onChange={(e) => {
                    formik.setFieldValue("fullName", e.target.value);
                    // formik.setFieldValue("age", 22);
                  }}
                  required={true}
                  placeholder="Enter Full Name"
                  style={{ borderRadius: "5px" }}
                ></FormikInput>
                <FormikInput
                  name="age"
                  label="Age"
                  inputType="number"
                  onChange={(e) => {
                    formik.setFieldValue("age", e.target.value);
                  }}
                  required={true}
                  disabled={true}
                  placeholder="Enter Age"
                  style={{ borderRadius: "5px" }}
                ></FormikInput>
                <FormikInput
                  name="subject"
                  label="Subject"
                  inputType="text"
                  onChange={(e) => {
                    formik.setFieldValue("subject", e.target.value);
                  }}
                  required={true}
                  placeholder="Enter Subject"
                  style={{ borderRadius: "5px" }}
                ></FormikInput>
                <FormikTextArea
                  name="description"
                  label="Description"
                  onChange={(e) => {
                    formik.setFieldValue("description", e.target.value);
                  }}
                  required={true}
                  placeholder="Enter Description"
                  style={{ borderRadius: "5px" }}
                ></FormikTextArea>
                <FormikSelect
                  name="country"
                  label="Country"
                  onChange={(e) => {
                    formik.setFieldValue("country", e.target.selec);
                  }}
                  options={countries}
                  required={true}
                  style={{ borderRadius: "5px" }}
                ></FormikSelect>
                <FormikRadio
                  name="gender"
                  label="Gender"
                  onChange={(e) => {
                    formik.setFieldValue("gender", e.target.value);
                  }}
                  options={genders}
                  required={true}
                  style={{ borderRadius: "5px" }}
                ></FormikRadio>

                <FormikCheckbox
                  name="isMarried"
                  label="Is Married"
                  required={true}
                  onChange={(e) => {
                    formik.setFieldValue("isMarried", e.target.value);
                  }}
                  style={{ borderRadius: "5px" }}
                ></FormikCheckbox>
                <button type="submit">Submit</button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </>
  );
};

export default FormikForm;
