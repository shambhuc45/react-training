import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import "./styles.css";
import { BrowserRouter } from "react-router-dom";
// import Project from "./Project.jsx";
import { Provider } from "react-redux";
import { store } from "./app/store.js";
import Project2 from "./Project2.jsx";
// import App from "./App";
// import MyApp from "./MyApp";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      {/* <React.StrictMode> */}
      {/* <App /> */}
      {/* <MyApp></MyApp> */}
      {/* </React.StrictMode> */}
      {/* <Project></Project> */}
      <Project2></Project2>
    </BrowserRouter>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
